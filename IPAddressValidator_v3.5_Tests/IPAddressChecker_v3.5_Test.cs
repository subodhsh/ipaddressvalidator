﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IPAddressChecker;

namespace Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class IPAddressChecker_v35_Test
    {
        public IPAddressChecker_v35_Test()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void PositiveScenario_Check1_Test()
        {
            //Arrange

           bool expected = true;
           bool actual;
           string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
           string VisitorsIPAddress="192.168.1.3";
           char Delimiter='|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
        public void PositiveScenario_Check2_Test()
        {
            //Arrange

            bool expected = true;
            bool actual;
            string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
            string VisitorsIPAddress = "192.58.22.3";
            char Delimiter = '|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
        public void PositiveScenario_Check3_Test()
        {
            //Arrange

            bool expected = true;
            bool actual;
            string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
            string VisitorsIPAddress = "192.168.4.8";
            char Delimiter = '|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
        public void PositiveScenario_Check4_Test()
        {
            //Arrange

            bool expected = true;
            bool actual;
            string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
            string VisitorsIPAddress = "192.32.7.23";
            char Delimiter = '|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
        public void PositiveScenario_Check5_Test()
        {
            //Arrange

            bool expected = true;
            bool actual;
            string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
            string VisitorsIPAddress = "192.23.44.45";
            char Delimiter = '|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
        public void PositiveScenario_Check6_Test()
        {
            //Arrange

            bool expected = true;
            bool actual;
            string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
            string VisitorsIPAddress = "192.23.44.78";
            char Delimiter = '|';


            // Act
            IPAddressValidator testclass = new IPAddressValidator();
            actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
            Assert.AreEqual<bool>(expected, actual);

        }

         [TestMethod]
         public void PositiveScenario_Check7_Test()
         {
             //Arrange

             bool expected = true;
             bool actual;
             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|192.168.24.26.*.*|201.25.24-28.32-39.58.*|";
             string VisitorsIPAddress = "201.25.25.33.58.9";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();
             actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
             Assert.AreEqual<bool>(expected, actual);

         }


         [TestMethod]
         public void NegativeScenario_Check1_Test()
         {
             //Arrange

             bool expected = false;
             bool actual;
             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|";
             string VisitorsIPAddress = "192.23.21.36";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();
             actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
             Assert.AreEqual<bool>(expected, actual);

         }


         [TestMethod]
         public void NegativeScenario_Check2_Test()
         {
             //Arrange

             bool expected = false;
             bool actual;
             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|192.23.22-87.45-*|";
             string VisitorsIPAddress = "192.58.21.36";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();
             actual = testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
             Assert.AreEqual<bool>(expected, actual);

         }

         [TestMethod]
        [ExpectedException(typeof(ArgumentException),"IPAddressRange not a Valid IPAddress")]
         public void NegativeScenario_Check3_Test()
         {
             //Arrange

             ArgumentException expected = new ArgumentException("IPAddressRange not a Valid IPAddress"); ;
            
             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|852.23.22-87.45-*|";
             string VisitorsIPAddress = "192.58.21.36";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();
            
             testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);
                 
            
            

         }


         [TestMethod]
         [ExpectedException(typeof(ArgumentException), "Visitor IP Address not valid")]
         public void NegativeScenario_Check4_Test()
         {
             //Arrange

            
             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|852.23.22-87.45-*|";
             string VisitorsIPAddress = "192.58.21.889";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();

             testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);




         }


         [TestMethod]
         [ExpectedException(typeof(ArgumentException), "IPAddressRange not a Valid IPAddress")]
         public void NegativeScenario_Check5_Test()
         {
             //Arrange

             ArgumentException expected = new ArgumentException("Visitor IP Address not valid"); ;

             string IPAddressRange = "|192.168.1.3|192.58.22.*|192.168.1-7.*|192.32.*.*|52.23.22-87.45-*|";
             string VisitorsIPAddress = "192.58.21.36.266";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();

             testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);




         }


         [TestMethod]
         [ExpectedException(typeof(ArgumentException), "IPAddressRange not a Valid IPAddress")]
         public void NegativeScenario_Check6_Test()
         {
             //Arrange


             string IPAddressRange = "|192.168.1.322|192.58.22.*|192.168.1-7.*|192.32.*.*|52.23.22-87.45-*|";
             string VisitorsIPAddress = "192.58.21.88.99";
             char Delimiter = '|';


             // Act
             IPAddressValidator testclass = new IPAddressValidator();

             testclass.Validate(IPAddressRange, VisitorsIPAddress, Delimiter);




         }
    }
}
