# README #

The Main purpose of this library is to validate whether a given IP Address falls in the given IP Address range specified by wild card characters.

For Example -

Given a IP Address range - **|192.168.1.3|192.58.22.\*|192.168.17.\*|192.32.\*.\*|192.23.22-87.45-\*|**
This library will help you check if **192.168.32.56** exists in the IP Address range mentioned above.

For More Info, you can go through my blog post [here](http://www.blog.subodhshetty.in/csharp/validate-ip-address-range/)-


### How do we pattern match a given IP address against a range ? ###

Pattern matching is done using the following two meta characters –

\* = For wild card mapping. i.e. any-value of IP segment is acceptable

– = To specify range.

** Example-1 **

Let say, you want to check if a given IP Address 201.23.45.22 falls between the IP Address range 201.22.0.0 to 201.22.45.24, you can assign the check variable as  **201.22.0-45.0-24**

** Example-2 **

Again, checking if given IP Address = 192.24.68.25 falls between the IPAddress range = 192.168.0.0 to 192.168.255.255,  you can assign the check variable as ** 192.168.\*.\* **

** Example-3 **

If you want to check against multiple IP address ranges like 192.168.22.56 to 192.168.22.87 OR between 201.22.25.24 – 201.66.25.56 then you can assign the check variable as  ** 192.168.22.56-87|201.22-66.25.54-56**

### Code Sample ###

```
string IP_Address_Range = "|192.168.1.3|192.58.22.*|192.168.17.*|192.32.*.*|192.23.22-87.45-*|";

string visitors-ip-address = "192.168.1.3";`

bool is-my-ip-address-within-the-mentioned-range = 
IPAddressValidator.Validate(IP_Address_Range,visitors-ip-address,"|");

if(is-my-ip-address-within-the-mentioned-range)
{
// Means My IP Address falls in the given IPAddressRange
}
else
{
// It Does not fall in the given IP Address range
}
```

### Does it support IPv6 as well ?###

YES. It supports IPv6 as well. In-fact, it goes a step forward. You can specify IPAddress Ranges in both IPv4 and IPv6 like “192.168.22.56-87|201.22-66.25.54-56|192.168.2.3.5.*” and still it would be able to validate whether 192.168.2.3.5.7 falls in the given IP Address range or no.